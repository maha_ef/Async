import asyncio


@asyncio.coroutine
def print_nums():
    pass


@asyncio.coroutine
def print_time():
    pass


@asyncio.coroutine
def main():
    pass


if __name__ == '__main__':
    main()
