

def coroutine(func):
    def inner(*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g
    return inner


class Blablaexception(Exception):
    pass


# yield from has initialization with g.send(None)
def subgen():
    while True:
        try:
            message = yield
        except StopIteration:
            break
        else:
            print('....', message)
    return 'Returned from subgen()'


@coroutine
def delegator(g):
    while True:
        try:
            data = yield
            g.send(data)
        except Blablaexception as e:
            print('We are in delegator exception, sending to subgen')
            g.throw(e)


@coroutine
def delegator_effective(g):
    result = yield from g
    print(result)
