from inspect import getgeneratorstate


def coroutine(func):
    def inner(*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g
    return inner


def subgen():
    x = 'Ready to accept message'
    message = yield x
    print('Subgen received: ', message)


def average():
    count = sum = 0
    average = None
    while True:
        try:
            x = yield average
        except StopIteration:
            print('Done')
        else:
            count += 1
            sum += x
            average = round(sum / count, 2)


@coroutine
def average_decorated():
    count = sum = 0
    average = None
    while True:
        try:
            x = yield average
        except StopIteration:
            print('Done')
        else:
            count += 1
            sum += x
            average = round(sum / count, 2)


@coroutine
def average_decorated_returning():
    count = sum = 0
    average = None
    while True:
        try:
            x = yield
        except StopIteration:
            print('Done')
            break
        else:
            count += 1
            sum += x
            average = round(sum / count, 2)

    return average


print('First just watch')
g = subgen()
try:
    print('Sending string "abcdef"')
    g.send('abcdef')
except TypeError:
    print('Error: you need to send None first to inirialize generator')
    print('Current status: ', getgeneratorstate(g))

print(g.send(None))
print('None was sent')
print('Current status: ', getgeneratorstate(g))

try:
    print('Sending string "Ok"')
    print(g.send('Ok'))
except:
    print('Here was error')

print('\n\nNow average')
g = average()
print('g.send(None) to inirialize')
g.send(None)
print('g.send(5)  Average is', g.send(5))
print('g.send(4)  Average is', g.send(4))
print('g.send(9)  Average is', g.send(9))
print('g.throw(StopIteration)  Result average is', g.throw(StopIteration))

print('\n\nNow average decorated')
g = average_decorated()
print('g.send(None) to inirialize IS NOT NEEDED ANYMORE (Auto)')
print('Status is', getgeneratorstate(g), 'already')
print('g.send(5)  Average is', g.send(5))
print('g.send(4)  Average is', g.send(4))
print('g.send(9)  Average is', g.send(9))
print('g.throw(StopIteration)  Result average is', g.throw(StopIteration))


print('\n\nNow average decorated returning final value')
print('Here we don\'t yield average on every step, only return in exception' )
g = average_decorated_returning()
print('g.send(None) to inirialize IS NOT NEEDED ANYMORE (Auto)')
print('Status is', getgeneratorstate(g), 'already')
print('g.send(5)')
g.send(5)
print('g.send(4)')
g.send(4)
print('g.send(9)')
g.send(9)

try:
    g.throw(StopIteration)
except StopIteration as e:
    print('Average is', e.value)
